(function () {

  var map;
  var islandsList;
  var markerCollection =  [];

  var ICONS = {
    'UNSELECTED': 'img/palm-tree.png',
    'SELECTED': 'img/palm-tree-select.png'
  };

  function initialise() {
    var mapOptions = {
      center: new google.maps.LatLng(-41.128314, 174.944385),
      zoom: 9
    };
    map = new google.maps.Map(document.querySelector("#islands-map"), mapOptions);
    
    islandsList = document.querySelector("#islands-list");

    var resetMapButton = document.createElement("div");
    resetMapButton.innerHTML = "<button>Show All Islands</button>";
    map.controls[google.maps.ControlPosition.TOP].push(resetMapButton);
    resetMapButton.addEventListener("click", function() {
      // deselectAllMarkers();
      map.setCenter(mapOptions.center);
      map.setZoom(mapOptions.zoom);
    })


    loadIslandsJSON();
  }

  google.maps.event.addDomListener(window, 'load', initialise);

  function loadIslandsJSON() {
  jQuery.getJSON('js/islands.json', processIslandsJSON);
  }
  
  function processIslandsJSON (islands) {
    console.log(islands);

    // sort islands array from north to south
    islands.sort(function (islandA, islandB) {
      if (islandA.lat < islandB.lat) {
        return 1; // A comes first - correct order
      } else {
        return -1; // B comes first - reverse the order
      }
    });

    islandsList.innerHTML = "";
        
    for (var i = 0; i < islands.length; i+=1) {
      var island = islands[i];
      addMarker(island);
      
    }
  }

  function addMarker(island) {
    var marker = new google.maps.Marker({
        'map': map,
        'position': new google.maps.LatLng(island.lat, island.lng),
        'title': island.name,
        'icon': ICONS.UNSELECTED
      });

      markerCollection.push(marker);

      var listItem =document.createElement('li');
        listItem.innerHTML = "<a href='#'>" + island.name + "</a>";
        islandsList.appendChild(listItem);

        var infoWindow = new google.maps.InfoWindow({
          'content': "<div><h3>" + island.name + "</h3><p>" + island.content + "</p></div>"
        });

        listItem.addEventListener("click", function (evt) {
          evt.preventDefault();
          selectMarker(marker, listItem);
        });
        
        google.maps.event.addDomListener(marker, "click", function(){
        selectMarker(marker, listItem);
        infoWindow.open(map,marker);
      });
    }
    
    function selectMarker(marker, listItem) {
      deselectAllMarkers();
          marker.setIcon(ICONS.SELECTED);
          marker.setZIndex(1000);
          map.setZoom(12);
          map.panTo(marker.getPosition());
          listItem.className = "active";

    }

    function deselectAllMarkers() {
      for (var i = 0; i < markerCollection.length; i += 1) {
          markerCollection[i].setIcon(ICONS.UNSELECTED);
          markerCollection[i].setZIndex(null);
      }
      listItemCollection = document.querySelectorAll('#islands-list > li');
      for (var i = 0; i < listItemCollection.length; i += 1) {
        listItemCollection[i].className = "";
      }
    }


})();